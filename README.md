# Temat
Modelowanie oprogramowania z wykorzystaniem języka UML

# Cel
Celem zajęć jest realizacja modelu przykładowego systemu w postaci zestawu diagramów UML.

# Narzędzia
Można korzystać z serwisu https://www.draw.io/

# Zadania

## Zadanie 1 - diagram przypadków użycia
Należy opracować diagram przypadków użycia paczkomatu. Diagram powinien przedstawiać zidentyfikowane przypadki użycia, zindentyfikowanych aktorów oraz relacje ich łączące.

## Zadanie 2 - diagram klas
Należy opracować diagram klas dla paczkomatu (na podstawie przygotowanego wcześniej podziału odpowiedzialności za pomocą kart CRC). Diagram powinien przedstawiać zidentyfikowane klasy, ich relacje oraz przykładowe atrybuty i operacje w klasach. Warto wstępnie ocenić zakres systemu, który chcemy modelować (próba ujęcia całego systemu może zająć zbyt wiele czasu).

## Zadanie 3 - diagram sekwencji
Należy opracować diagram sekwencji przedstawiający proces nadawania przesyłek (np. od rejestracji przesyłki do wyjęcia przesyłki przez kuriera z paczkomatu nadawczego). Uwaga na rozmiar paczek i chwilową zajętość paczkomatu.

## Zadanie 4 - diagram stanu
Należy opracować diagram stanu przedstawiający cykl życia przesyłki paczkomatowej, od rejestracji do doręczenia (odbioru).

## Zadanie 5 - diagram czynności	
Należy opracować diagram czynności opisujący nadanie przesyłki.

## Zadanie 6 - diagram komponentów oraz wdrożenia
Należy zaproponować architekturę systemu (na wysokim poziomie abstrakcji) wraz z koncepcją wdrożenia.